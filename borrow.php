<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>ระบบห้องสมุด</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="menuadmin.php">
                    
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">    
            <div class="header-content clearfix">
                
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                </div>
                <form action="details_book.php" method="GET">
                <div class="header-left">
                    <div class="input-group icons">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-transparent border-0 pr-2 pr-sm-3" id="basic-addon1"><i class="mdi mdi-magnify"></i></span>
                        </div>
                        <input type="text" class="form-control" name="search" placeholder="ค้นหาหนังสือ">
                    </div>
                </div>
                <div class="header-left">
                    <div class="input-group icons">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn mb-1 btn-primary">ค้นหา</button>
                        </div>      
                    </div>
                    
                </div>
            </form>
                <div class="header-right">
                    <ul class="clearfix">
                        
                        <li class="icons dropdown d-none d-md-flex">
                            <a href="javascript:void(0)" class="log-user"  data-toggle="dropdown">
                                <span>admin</span>  
                            </a>
                        </li>
                        <li class="icons dropdown">
                            <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                                <span class="activity active"></span>
                                <img src="images/user/1.png" height="40" width="40" alt="">
                            </div>
                            <div class="drop-down dropdown-profile   dropdown-menu">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li><a href="page-login.html"><i class="icon-key"></i> <span>Logout</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Dashboard</li>
                    <li>
                        <a class="has-arrow" href="view_member.php" aria-expanded="false">
                        <i class="icon-menu menu-icon"></i><span class="nav-text">ข้อมูลสมาชิก</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="view_book.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">ข้อมูลหนังสือ</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="view_librarian.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">ข้อมูลเจ้าหน้าที่</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="borrow.php" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">ข้อมูลการยืมหนังสือ</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="return.php" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">ข้อมูลการคืนหนังสือ</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="logout.php" aria-expanded="false">
                            <i class="icon-key menu-icon"></i><span class="nav-text">ออกจากระบบ</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">ข้อมูลการยืมหนังสือ</h4>
                                <a href="form_borrow.php"><button type="button" class="btn mb-1 btn-success btn-lg ">ยืมหนังสือ</button></a>
                                <div class="table-responsive"> 
                                    <table class="table table-bordered table-striped verticle-middle">
                                        <thead>
                                            <tr>
                                                <th scope="col">ชื่อหนังสือ</th>
                                                <th scope="col">ชื่อผู้ยืม</th>
                                                <th scope="col">ชื่อให้ยืม</th>
                                                <th scope="col">วันที่ยืม</th>
                                                <th scope="col">วันที่กำหนดคืน</th>
                                                <th scope="col">สถานะ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            include 'connect.php';
                                            $objCon = mysqli_connect($serverName,$userName,$userPassword,$dbName);
                                            // $strSQL="select *
                                            // from borrow inner join book on borrow.id_book = book.ID_Book 
                                            // inner join account on borrow.id_account = account.id
                                            // inner join account on borrow.id_account_lend = account.id";
                                        $strSQL="select * from borrow where status_borrow = 0";
                                            mysqli_query($objCon,"SET NAMES UTF8");
                                            $objQuery=mysqli_query($objCon,$strSQL);
                                            while($record=mysqli_fetch_array($objQuery))
                                            {   
                                                
                                                if($record['status_borrow']==0){$status='ยืม';}
                                                else if($record['status_borrow']==1){$status='ว่าง';}
                                                echo"
                                            <tr>
                                                <td>$record[id_book]</td>
                                                <td>$record[id_account]</td>
                                                <td>$record[id_account_lend]</td>
                                                <td>$record[borrow_date]</td>
                                                <td>$record[final_borrow]</td>
                                                <td>$status</td>
                                                <td>
                                                <a href='returnbook.php?id_edit=$record[id_borrow]'><button type='button' class='btn mb-1 btn-danger btn-lg'>คืนหนังสือ</button></a></td>
                                                
                                                </td>
                                            </tr>";} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright &copy; Designed & Developed by <a href="https://themeforest.net/user/quixlab">Quixlab</a> 2018</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

</body>

</html>