<?php
session_start();
ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>ระบบห้องสมุด</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">

</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="menuadmin.php">
                    
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">    
            <div class="header-content clearfix">
                
                <div class="nav-control">
               
            </div>
			</div></div></div></div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    
                </div>
            </div>
            <!-- row -->
			<div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
					<div class="card">
                            <div class="card-body">
	<?php
	
	$username=$_POST['user'];
	$password=$_POST['pass'];
	
	if($username=="" or $password==""){
		echo"<h4> ERROR กรุณาใส่ Username หรือ Password";
		exit();
		
		}

	include 'connect.php';
	$objCon = mysqli_connect($serverName,$userName,$userPassword,$dbName);
	$strSQL="select * from account where username ='$username' and password ='$password'";
	$objQuery=mysqli_query($objCon,$strSQL);
	$num=mysqli_fetch_array($objQuery);
	session_write_close();
	if($num['status'] == "librarian")
	{
		$_SESSION['sess_userid']=session_id();
		$_SESSION['sess_username']=$username;
		header('Location: menuadmin.php');
	}
	else if($num['status'] == "student")
	{
		$_SESSION['sess_userid']=session_id();
		$_SESSION['sess_username']=$username;
		header('Location: menuuser.php');
	}
	else{
		echo "<h3> Username และ Password ไม่ถูกต้อง";
	}
	?><div class="footer">
	
</div></div>
</div>
<!--**********************************
	Footer end
***********************************-->
</div>
<!--**********************************
Main wrapper end
***********************************-->

<!--**********************************
Scripts
***********************************-->
<script src="plugins/common/common.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/settings.js"></script>
<script src="js/gleek.js"></script>
<script src="js/styleSwitcher.js"></script>

</body>

</html>