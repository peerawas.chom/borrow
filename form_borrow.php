<!DOCTYPE html>
<html lang="en">
<?php
include 'connect.php';	

$objCon = mysqli_connect($serverName,$userName,$userPassword,$dbName);
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>ระบบห้องสมุด</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="./plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="./plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="./plugins/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="./plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
    <!-- Daterange picker plugins css -->
    <link href="./plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="./plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

</head>
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="menuadmin.php">
                    
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">    
            <div class="header-content clearfix">
                
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                </div>
                <form action="details_book.php" method="GET">
                <div class="header-left">
                    <div class="input-group icons">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-transparent border-0 pr-2 pr-sm-3" id="basic-addon1"><i class="mdi mdi-magnify"></i></span>
                        </div>
                        <input type="text" class="form-control" name="search" placeholder="ค้นหาหนังสือ">
                    </div>
                </div>
                <div class="header-left">
                    <div class="input-group icons">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn mb-1 btn-primary">ค้นหา</button>
                        </div>      
                    </div>
                    
                </div>
            </form>
                <div class="header-right">
                    <ul class="clearfix">
                        
                        <li class="icons dropdown d-none d-md-flex">
                            <a href="javascript:void(0)" class="log-user"  data-toggle="dropdown">
                                <span>admin</span>  
                            </a>
                        </li>
                        <li class="icons dropdown">
                            <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                                <span class="activity active"></span>
                                <img src="images/user/1.png" height="40" width="40" alt="">
                            </div>
                            <div class="drop-down dropdown-profile   dropdown-menu">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li><a href="page-login.html"><i class="icon-key"></i> <span>Logout</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Dashboard</li>
                    <li>
                        <a class="has-arrow" href="view_member.php" aria-expanded="false">
                        <i class="icon-menu menu-icon"></i><span class="nav-text">ข้อมูลสมาชิก</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="view_book.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">ข้อมูลหนังสือ</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="view_librarian.php" aria-expanded="false">
                            <i class="icon-menu menu-icon"></i><span class="nav-text">ข้อมูลเจ้าหน้าที่</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="borrow.php" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">ข้อมูลการยืมหนังสือ</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="return.php" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">ข้อมูลการคืนหนังสือ</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow" href="logout.php" aria-expanded="false">
                            <i class="icon-key menu-icon"></i><span class="nav-text">ออกจากระบบ</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="card-title">การยืมหนังสือ</h4>
                                <div class="basic-form">
                                    <form action="add_borrow.php" method="post">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text">ผู้ยืม</label>
                                            </div>
                                            <select class="custom-select" name='std_borrow'>
                                                <option selected="selected">เลือกชื่อผู้ยืม</option>
                                                <?php 
                                                $SQLstudent="select * from account where status like 'student'";
                                                mysqli_query($objCon,"SET NAMES UTF8");
                                                $objQuery=mysqli_query($objCon,$SQLstudent);
                                                while($record=mysqli_fetch_array($objQuery))
                                                { echo " <option  value='$record[id]'>$record[name]</option>    ";}
                                                ?>
                                            </select>
                                        </div>    

                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text">หนังสือ</label>
                                            </div>
                                            <select class="custom-select" name='book'>
                                                <option selected="selected">เลือกชื่อหนังสือ</option>
                                                <?php 
                                                $SQLbook="select * from book where status_book= 1";
                                                $bookQuery=mysqli_query($objCon,$SQLbook);
                                                while($book=mysqli_fetch_array($bookQuery))
                                                {  echo " <option value='$book[ID_Book]'>$book[Name_Book]</option>";}
                                                ?>
                                            </select>
                                        </div>    


                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text">ผู้ให้ยืม</label>
                                            </div>
                                            <select class="custom-select" name='librarian_lend'>
                                                <option selected="selected">เลือกชื่อผู้ให้ยืม</option>
                                                <?php 
                                                $SQLlibrarian="select * from account where status like 'librarian'";
                                                $lendQuery=mysqli_query($objCon,$SQLlibrarian);
                                                while($lend=mysqli_fetch_array($lendQuery))
                                                {   echo "<option value='$lend[id]'>$lend[name]</option>";}
                                                ?>
                                            </select>
                                        </div>    
                                        
                                        <div class="col-md-6">
                                            <label class="m-t-20">วันที่ยืม</label>
                                            <input type="text" class="form-control mydatepicker"  name="borrow_date" placeholder="" value="<?php echo date('d/m/y')?>" >
                                            <label class="m-t-40">กำหนดวันที่คืน</label>
                                            <input type="text" class="form-control" name="final_borrow" id="datepicker-autoclose" value=""  placeholder="<?php echo date('d/m/y')?>"> <span class="input-group-append"><span class="input-group-text"></span></span>
                                        </div>

                                        <br><a href="add_borrow.php"><button type="submit" class="btn mb-1 btn-primary btn-lg" >เพิ่ม</button></a>
                                                </div>
                                                
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright &copy; Designed & Developed by <a href="https://themeforest.net/user/quixlab">Quixlab</a> 2018</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

    <script src="./plugins/moment/moment.js"></script>
    <script src="./plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="./plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="./plugins/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
    <script src="./plugins/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
    <script src="./plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="./plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="./plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="./plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script src="./js/plugins-init/form-pickers-init.js"></script>

</body>
</html>