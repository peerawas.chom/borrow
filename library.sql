-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 08:30 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` int(10) NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student' COMMENT 'student / librarian'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `username`, `password`, `name`, `address`, `tel`, `status`) VALUES
(1, 'admin', '1234', 'admin', 'atc', 123456789, 'librarian'),
(2, 'user', '12345', 'user', 'atc', 1234567890, 'student'),
(3, 'mith', '1234', 'mith', 'thai', 988888, 'librarian'),
(4, 'new', '456', 'new', 'atc', 2147483647, 'student'),
(6, 'pon', 'mon', 'spy', 'ram2', 1234, 'student'),
(7, 'ten', 'ben', 'top', 'somrong', 1150, 'librarian'),
(9, 'sdasdsadsadsa', 'asdasd', 'หฟกฟกฟห', 'sadasd', 0, 'student');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `ID_Book` int(10) NOT NULL,
  `Name_Book` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Date_Print` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Author` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `Count_borrow_M` int(11) NOT NULL DEFAULT 7 COMMENT 'จำนวนวันยืมได้สำหรับสมาชิก',
  `Count_borrow_T` int(11) NOT NULL DEFAULT 14 COMMENT 'จำนวนวันยืมได้สำหรับเจ้าหน้าที่',
  `status_book` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`ID_Book`, `Name_Book`, `type`, `Date_Print`, `Author`, `price`, `Count_borrow_M`, `Count_borrow_T`, `status_book`) VALUES
(1, 'บรรณานุกรม', 'ความรู้', '2 ก.พ. 63', 'สมชาย ดีสุข', 350, 12, 24, 1),
(11, 'ภาษาไทย', 'เรียน', '2020-02-12', 'กัน', 0, 7, 14, 1),
(4, 'Eng', 'ภาษา', '1ม.ค.2020', 'มาย', 0, 7, 14, 1),
(13, 'C++', 'code', '2020-02-12', 'ไอมาย', 0, 7, 14, 0),
(14, 'JAVA', 'code', '2020-02-11', 'สมสี', 500, 30, 120, 1);

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE `borrow` (
  `id_borrow` int(11) NOT NULL COMMENT 'รหัสการยืมคืน',
  `id_account` int(11) NOT NULL COMMENT 'รหัสคนยืม',
  `id_book` int(11) NOT NULL COMMENT 'รหัสหนังสือ',
  `id_account_lend` int(11) NOT NULL COMMENT 'รหัสคนให้ยืม',
  `id_account_get` int(11) DEFAULT NULL COMMENT 'รหัสคนรับคืน',
  `borrow_date` date NOT NULL,
  `return_date` date DEFAULT NULL,
  `final_borrow` date NOT NULL,
  `status_borrow` int(11) NOT NULL DEFAULT 1 COMMENT '1 = ยืมได้ / 0 = ยังไม่คืน'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `borrow`
--

INSERT INTO `borrow` (`id_borrow`, `id_account`, `id_book`, `id_account_lend`, `id_account_get`, `borrow_date`, `return_date`, `final_borrow`, `status_borrow`) VALUES
(19, 4, 1, 1, 1, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(20, 2, 1, 1, 1, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(21, 2, 11, 1, 1, '2004-02-20', '2028-02-20', '0000-00-00', 1),
(22, 2, 11, 3, 3, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(23, 2, 11, 3, 3, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(24, 2, 1, 1, 1, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(25, 2, 1, 1, 1, '2004-02-20', '2028-02-20', '0000-00-00', 1),
(26, 2, 1, 1, 1, '2004-02-20', '2028-02-20', '0000-00-00', 1),
(27, 2, 1, 1, 0, '2004-02-20', '0000-00-00', '0000-00-00', 1),
(28, 2, 11, 1, 1, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(29, 2, 11, 3, 3, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(30, 2, 4, 3, 3, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(31, 4, 11, 1, 1, '2028-02-20', '2028-02-20', '0000-00-00', 1),
(32, 6, 13, 1, 0, '2028-02-20', '0000-00-00', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lend`
--

CREATE TABLE `lend` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_lend_id` int(11) DEFAULT NULL COMMENT 'ผู้ให้ยืม',
  `user_return_lend` int(11) DEFAULT NULL COMMENT 'ผู้รับคืน',
  `lend_date` date NOT NULL,
  `return_date` date NOT NULL,
  `is_return_date` date DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lend`
--

INSERT INTO `lend` (`id`, `book_id`, `user_id`, `user_lend_id`, `user_return_lend`, `lend_date`, `return_date`, `is_return_date`, `status`) VALUES
(1, 2, 3, 1, 1, '2020-02-26', '2020-03-04', '2020-02-26', 2),
(2, 2, 2, 1, 1, '2020-02-26', '2020-03-04', '2020-02-26', 2),
(3, 2, 2, 1, 1, '2020-02-26', '2020-03-04', '2020-02-26', 2),
(4, 2, 2, 1, 1, '2020-02-26', '2020-03-04', '2020-02-26', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`ID_Book`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`id_borrow`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `ID_Book` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `borrow`
--
ALTER TABLE `borrow`
  MODIFY `id_borrow` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสการยืมคืน', AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
